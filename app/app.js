var app = angular.module('miApp', ['ngRoute', 'ui.bootstrap', 'checklist-model', 'ngAnimate'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'partials/home.html'
            })
            .when('/test', {
                controller: 'TestController',
                templateUrl: 'partials/test.html'
            })
            .when('/sede', {
                controller: 'SedeController',
                templateUrl: 'partials/sede/index.html'
            })
            .when('/funcionario', {
                controller: 'UsuarioController',
                templateUrl: 'partials/usuario/index.html'
            })
            .when('/equipo', {
                controller: 'EquipoController',
                templateUrl: 'partials/equipo/index.html'
            })
            .when('/alerta', {
                controller: 'AlertaController',
                templateUrl: 'partials/alerta/index.html'
            })
            .when('/tecnica', {
                controller: 'TecnicaController',
                templateUrl: 'partials/tecnica/index.html'
            })
            .when('/tecnica/equipos/:id', {
                controller: 'TecnicaEquiposController',
                templateUrl: 'partials/tecnica/equipos.html'
            })
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'partials/login.html'
            })
            .when('/logout', {
                controller: 'LogoutController',
                template: 'bye'
            })
            .when('/misequipos', {
                controller: 'MisEquiposController',
                templateUrl: 'partials/misequipos/index.html'
            })
            .when('/misequipos/enviar', {
                controller: 'MisEquiposEnviarController',
                templateUrl: 'partials/misequipos/enviar.html'
            })
            .when('/misequipos/resumen/:id', {
                controller: 'MisEquiposResumenController',
                templateUrl: 'partials/misequipos/resumen.html'
            })
            .when('/remisiones', {
                controller: 'RemisionesController',
                templateUrl: 'partials/remisiones/index.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .factory('httpPostFactory', function ($http) {
        return function (file, data, callback) {
            $http({
                url: file,
                method: "POST",
                data: data,
                headers: {'Content-Type': undefined}
            })
            .then(function (response) {
                callback(response);
            });
        };
    })
;
