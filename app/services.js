app
        .service('requestService', function ($http) {
                this.send = function (path, method, payload) {
                    method = typeof method !== 'undefined' ?  method : 'get';
                    payload = typeof payload !== 'undefined' ?  payload : {};
                    switch (method) {
                        case "get":
                            return $http.get(URL_API + '/' + path, {
                                headers: {
                                    'token': sessionStorage.getItem('token')
                                }
                            });
                            break;
                        case "post":
                            return $http.post(URL_API + '/' + path, payload,{
                                headers: {
                                    'token': sessionStorage.getItem('token')
                                }
                            });
                            break;
                        case "put":
                            return $http.put(URL_API + '/' + path, payload,{
                                headers: {
                                    'token': sessionStorage.getItem('token')
                                }
                            });
                            break;
                        case "delete":
                            return $http.delete(URL_API + '/' + path, {
                                headers: {
                                    'token': sessionStorage.getItem('token')
                                }
                            });
                            break;
                    }
                    
                };
            })
        .service('loadingService', [function(){
            var loading = {};
            return {
                registrar: function(api){
                    loading = api;
                },
                on: function() {
                    loading.on();
                },
                off: function() {
                    loading.off();
                }
            };
        }])
        .service('configService', function (requestService, $location, badgeEquiposService, storageService) {

                var config = {};
                return {
                    registrar: function (data) {
                        config.menu = data;
                    },
                    isAdmin: function () {
                        config.menu.isAdmin();
                    },
                    isUser: function () {
                        config.menu.isUser();
                    },
                    isLogged: function (state) {
                        config.menu.isLogged(state);
                    },
                    checkLogin: function () {
                        badgeEquiposService.setPendientes(storageService.len('equiposSeleccionados'));
                        requestService.send('api/v1/checkLogin?token=' + sessionStorage.getItem('token'))
                            .then(
                                function (data) {
                                    if (data && data.data && data.data.esLogged === true) {
                                        config.menu.isLogged(true);
                                        if (data.data.esAdmin) {
                                            config.menu.isAdmin();
                                        } else {
                                            config.menu.isUser();
                                        }
                                        
                                        if (data.data.cambiarPass) {
                                            config.menu.cambiarPass(true);
                                        } else {
                                            config.menu.cambiarPass(false);
                                        }
                                    } else {
                                        config.menu.isUser();
                                        config.menu.isLogged(false);
                                        $location.path('/login');
                                    }
                                }
                            )
                            ;
                    }
                };

            })
            .service('badgeMailService', function() {
                var self = {};
                return {
                    registrar : function (api) {
                        self = api;
                    },
                    setPendientes: function(n) {
                        self.setPendientes(n)
                    }
                };
            })
            .service('badgeEquiposService', function() {
                var self = {};
                return {
                    registrar : function (api) {
                        self = api;
                    },
                    setPendientes: function(n) {
                        self.setPendientes(n)
                    }
                };
            })
            .service('storageService', function() {
                var service = {
                    set : function (key, object) {
                        return sessionStorage.setItem(key, JSON.stringify(object));
                    },
                    get : function (key) {
                        var retorno = JSON.parse(sessionStorage.getItem(key));
                        if (retorno !== null) {
                            return retorno;
                        } else {
                            return [];
                        }
                    },
                    len : function(key) {
                        return service.get(key).length;
                    }
                };
                return service;
            })
;