/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


app
        .directive('menu', function (configService, requestService, $uibModal) {
                return {
                    restrict: 'E',
                    templateUrl: 'partials/menu.html',
                    controller: function ($scope, configService) {
                        $scope.mostrarCondiciones = function () {
                            $uibModalInstance = $uibModal.open({
                                templateUrl: 'partials/condiciones.html',
                                controller: 'condicionesController',
                                keyboard: false,
                                backdrop: true, //'static'
                                scope: $scope
                            });
                            $uibModalInstance.result.then(
                                function (data) {
                                },
                                function (data) {
                                }
                            );
                        };
                        $scope.cambiarpassword = function () {
                            $uibModalInstance = $uibModal.open({
                                templateUrl: 'partials/usuario/password.html',
                                controller: 'UsuarioPasswordController',
                                keyboard: false,
                                backdrop: true, //'static'
                                scope: $scope
                            });
                            $uibModalInstance.result.then(
                                function (data) {
                                    requestService.send('api/v1/usuario/password', 'put', {password: data, cambiarPass: false})
                                        .then(
                                            function (data) {
                                                notificacion('success', data.data.message);
                                            },
                                            function (data) {
                                                if (data.data && data.data.message) {
                                                    notificacion('error', data.data.message);
                                                } else {
                                                    notificacion('error', 'A connection error was detected.');
                                                }
                                            }
                                        );

                                },
                                function (data) {
                                }
                            );
                        };
                        var api = {
                            isAdmin: function () {
                                $scope.isAdmin = true;
                            },
                            isUser: function () {
                                $scope.isAdmin = false;
                            },
                            isLogged: function (state) {
                                $scope.isLogged = state;
                            },
                            cambiarPass: function(val) {
                                $scope.cambiarPass = val;
                            }
                        };
                        configService.registrar(api);
                    }
                };
            }
        )
        .directive('noautorizado', ['configService', 'requestService', function (configService, requestService) {
                return {
                    restrict: 'A',
                    templateUrl: 'partials/no-autorizado.html',
                    controller: function () {
                    }
                };
            }
        ])
        .directive('loading', ['loadingService', function(loadingService){
                return {
                    restrict: 'E',
                    template: ' <div id="app-spinner-container" ng-show="loading">' + 
                                    '<div id="app-spinner">' +
                                    '    <span class="demo-text spindle-lg"></span>' +
                                    '    <span class="demo-text spindle-md"></span>' +
                                    '    <span class="demo-text spindle-sm"></span>' +
                                    '    <span class="demo-text spindle-xs"></span>' +
                                    '</div>' +
                                '</div>',
                    controller: function($scope, loadingService) {
                        var api = {
                            on: function(){
                                $scope.loading = true;
                            },
                            off: function(){
                                $scope.loading = false;
                            }
                        };
                        loadingService.registrar(api);
                    }
                };
        }])
        .directive('myDirective', function (httpPostFactory) {
            return {
                restrict: 'A',
                scope: false,
                link: function ($scope, element, attr) {
                    element.bind('change', function () {
                        var formData = new FormData();
                        formData.append('file', element[0].files[0]);
                        httpPostFactory(URL_STORAGE , formData, function (callback) {
                            // recieve image name to use in a ng-src 
                            $scope.imagenes(callback.data);
                        });
                    });
                }
            };
        })
        .directive('badgeMail', function (badgeMailService, $location) {
                return {
                    restrict: 'E',
                    templateUrl: 'partials/common/badgeMail.html',
                    controller: function ($scope) {
                        var api = {
                            setPendientes: function (n) {
                                $scope.numeroMensajes = n;
                            }
                        };
                        badgeMailService.registrar(api);
                        $scope.goMensajes = function () {
                            $location.path('/mensaje');
                        };
                    }
                };
            }
        )
        .directive('badgeEquipos', function (badgeEquiposService, $location) {
                return {
                    restrict: 'E',
                    templateUrl: 'partials/common/badgeEquipos.html',
                    controller: function ($scope) {
                        var api = {
                            setPendientes: function (n) {
                                $scope.numeroEquipos = n;
                            }
                        };
                        badgeEquiposService.registrar(api);
                        $scope.goEquipos = function () {
                            $location.path('/misequipos/enviar');
                        };
                    }
                };
            }
        )
        ;