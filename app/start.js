var app = angular.module('miApp', ['ngRoute', 'ui.bootstrap', 'checklist-model', 'ngAnimate'])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'partials/home.html'
            })
            .when('/test', {
                controller: 'TestController',
                templateUrl: 'partials/test.html'
            })
            .when('/sede', {
                controller: 'SedeController',
                templateUrl: 'partials/sede/index.html'
            })
            .when('/sede/:id', {
                controller: 'SedeEditController',
                templateUrl: 'partials/sede/edit.html'
            })
            .when('/funcionario', {
                controller: 'UsuarioController',
                templateUrl: 'partials/usuario/index.html'
            })
            .when('/funcionario/:id', {
                controller: 'UsuarioEditController',
                templateUrl: 'partials/usuario/edit.html'
            })
            .when('/equipo', {
                controller: 'EquipoController',
                templateUrl: 'partials/equipo/index.html'
            })
            .when('/equipo/:id', {
                controller: 'EquipoEditController',
                templateUrl: 'partials/equipo/edit.html'
            })
            .when('/alerta', {
                controller: 'AlertaController',
                templateUrl: 'partials/alerta/index.html'
            })
            .when('/tecnica', {
                controller: 'TecnicaController',
                templateUrl: 'partials/tecnica/index.html'
            })
            .when('/tecnica/:id', {
                controller: 'TecnicaEditController',
                templateUrl: 'partials/tecnica/edit.html'
            })
            .when('/tecnica/equipos/:id', {
                controller: 'TecnicaEquiposController',
                templateUrl: 'partials/tecnica/equipos.html'
            })
            .when('/misequipos', {
                controller: 'MisEquiposController',
                templateUrl: 'partials/misequipos/index.html'
            })
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'partials/login.html'
            })
            .when('/logout', {
                controller: 'LogoutController',
                template: 'bye'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .factory('httpPostFactory', function ($http) {
        return function (file, data, callback) {
            $http({
                url: file,
                method: "POST",
                data: data,
                headers: {'Content-Type': undefined}
            })
            .then(function (response) {
                callback(response);
            });
        };
    })
;