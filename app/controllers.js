app
    .controller("HomeController", 
        function (configService, $scope, $uibModal, requestService) {
            configService.checkLogin();
            setTimeout(function(){
                if ($scope.cambiarPass) {
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/usuario/password.html',
                            controller: 'UsuarioPasswordController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(
                            function (data) {
                                requestService.send('api/v1/usuario/password', 'put', {password: data, cambiarPass: false})
                                    .then(
                                        function (data) {
                                            notificacion('success', data.data.message);
                                        },
                                        function (data) {
                                            if (data.data && data.data.message) {
                                                notificacion('error', data.data.message);
                                            } else {
                                                notificacion('error', 'A connection error was detected.');
                                            }
                                        }
                                    );
                                
                            },
                            function (data) {
                            }
                        ); 
                } 
            }, 3000);
        }
    )
    .controller("UsuarioPasswordController", function($scope){
        $scope.guardar = function () {
            $uibModalInstance.close($scope.password);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller("condicionesController", function($scope){
        $scope.ok = function () {
            $uibModalInstance.close();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller("ConfirmacionDeleteController", function($scope){
        $scope.ok = function () {
            $uibModalInstance.close();
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller("LoginController", ['configService', 'requestService', '$scope', '$http', '$location',
            function (configService, requestService, $scope, $http, $location) {
                sessionStorage.removeItem('token');
                $scope.user = {};
                $scope.submitForm = function () {
                    $http({
                        method: 'POST',
                        url: URL_API + '/api/v1/login',
                        data: $scope.user,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    })
                    .then(
                        function (data) {
                            if (data.data && data.data.status && data.data.status === 'ok') {
                                notificacion('success', data.data.message);
                                sessionStorage.setItem('token', data.data.token);
                                $location.path('/');
                            }
                        }, function (data) {
                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                            }
                            $location.path('/login');
                        }
                    );
                };
            }
        ])
        .controller("LogoutController", ['configService', '$scope', '$location',
            function (configService, $scope, $location) {
                sessionStorage.removeItem('token');
                sessionStorage.removeItem('equiposSeleccionados');
                
                $scope.user = {};
                configService.checkLogin();
                $location.path('/login');
            }
        ])
    .controller("AlertaNoRecepcionController", function(loadingService, requestService, $scope, $route){
            $scope.okNoRecepcion = function () {
                data = [$scope.dia1, $scope.dia2, $scope.dia3];
                requestService.send('api/v1/alerta/update', 'post', { data: data, entityType: $scope.alertaCodigo })
                .then(
                    function (data) {
                        notificacion('success', data.data.message);
                        loadingService.off();
                        $route.reload();
                    },
                    function (data) {
                        if (data.data && data.data.message) {
                            notificacion('error', data.data.message);
                        } else {
                            notificacion('error', 'A connection error was detected.');
                        }
                        loadingService.off();
                    }
                );                
                $uibModalInstance.close($scope.alerta);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
            //setTimeout(function() {
                $scope.dia1 = $scope.alertaData.configs[0].dia;
                $scope.dia2 = $scope.alertaData.configs[1].dia;
                $scope.dia3 = $scope.alertaData.configs[2].dia;
            //}, 500);
            
        })
        .controller("AlertaEquiposACargoController", function(loadingService, requestService, $scope){
            $scope.okEquiposACargo = function () {
                $uibModalInstance.close($scope.alerta);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
            $scope.options = {
                showWeeks: false
            };
        })
        .controller("AlertaController",
                function (loadingService, configService, requestService, $scope, $route, $uibModal) {
                    loadingService.on();
                    configService.checkLogin();
                    $scope.openModalNew = function (alertaCodigo) {
                        $scope.alertaCodigo = alertaCodigo;
                        
                        
                        requestService.send('api/v1/alerta/' + alertaCodigo)
                        .then(
                            function (data) {
                                $scope.alertaData = data.data;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                    //$location.path('/login')
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                    //$location.path('/login')
                                }
                                loadingService.off();
                            }
                        );
                        
                        
                        
                        var alertaTemplate = "";
                        switch (alertaCodigo) {
                            case "no_recepcion":
                            case "equipos_a_cargo":
                                alertaTemplate = alertaCodigo;
                                alertaController = (alertaCodigo === 'no_recepcion') ? 'NoRecepcion' : 'EquiposACargo';
                                $uibModalInstance = $uibModal.open({
                                    templateUrl: 'partials/alerta/' + alertaTemplate + '.html',
                                    controller: 'Alerta' + alertaController + 'Controller',
                                    keyboard: false,
                                    backdrop: true, //'static'
                                    scope: $scope
                                });
                                $uibModalInstance.result.then(
                                    function (alerta) {
                                    },
                                    function (alerta) {
                                    }
                                );
                                break;
                            default:
                                notificacion('error', 'Tipo de alerta no reconocida.');
                                break;
                        }
                        
                                
                    };
                    $scope.openModalFecha = function (alertaCodigo) {
                        $scope.alertaCodigo = alertaCodigo;
                                $uibModalInstance = $uibModal.open({
                                    templateUrl: 'partials/alerta/no_recepcion.html',
                                    controller: 'AlertaNoRecepcionController',
                                    keyboard: false,
                                    backdrop: true, //'static'
                                    scope: $scope
                                });
                                $uibModalInstance.result.then(
                                    function (alerta) {
                                    },
                                    function (alerta) {
                                    }
                                );        
                    };
                    requestService.send('api/v1/alerta')
                        .then(
                            function (data) {
                                $scope.entities = data.data;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                    //$location.path('/login')
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                    //$location.path('/login')
                                }
                                loadingService.off();
                            }
                        );
                }
        )
        .controller("EquipoNewController", function(loadingService, requestService, $scope){
            var images = new Array();
            $scope.ok = function () {
                //$scope.equipo.fotos=images;
                $uibModalInstance.close({equipo: $scope.equipo, fotos: images});
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
            $scope.imagenes = function (img) {
                images.push(img);
            };
        })
        .controller("EquipoDetailController", function(loadingService, requestService, $scope){
            var images = new Array();
            $scope.url_imgs = URL_API;
            requestService.send('api/v1/equipo/' + $scope.equipoId)
                .then(
                    function (data) {
                        $scope.entity = data.data;
                        loadingService.off();
                    },
                    function (data) {

                        if (data.data && data.data.message) {
                            notificacion('error', data.data.message);
                            //$location.path('/login')
                        } else {
                            notificacion('error', 'A connection error was detected.');
                            //$location.path('/login')
                        }
                        loadingService.off();
                    }
                );
            requestService.send('api/v1/equipo/mis/equipos/trazabilidad/' + $scope.equipoId)
                .then(
                        function (data) {
                            $scope.movimientos = data.data;
                            loadingService.off();
                        },
                        function (data) {
                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                ); 
            $scope.ok = function () {
                //$scope.equipo.fotos=images;
                $uibModalInstance.close({equipo: $scope.equipo, fotos: images});
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .controller("EquipoController",
                function (loadingService, configService, requestService, $scope, $route, $uibModal) {
                    loadingService.on();
                    configService.checkLogin();
                    $scope.admin = false;
                    $scope.openModalBorrar = function (entityId) {
                        $scope.entityId = entityId;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/common/confirmacionDelete.html',
                            controller: 'ConfirmacionDeleteController',
                            size: 'sm',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function () {
                            loadingService.on();
                            requestService.send('api/v1/equipo/delete/' + $scope.entityId, 'delete')
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalNew = function () {
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/equipo/new.html',
                            controller: 'EquipoNewController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (equipo) {
                            loadingService.on();
                            requestService.send('api/v1/equipo/new', 'post', { data: equipo })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                            if (data.data.errors) {
                                                data.data.errors.forEach(function(error){
                                                    notificacion('error', error.message);
                                                });
                                            }
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalDetalle = function (id) {
                        $scope.equipoId = id;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/equipo/detail.html',
                            controller: 'EquipoDetailController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (equipo) {
                            loadingService.on();
                            }, function () {}
                        );
                    };
                    $scope.openModalEdit = function (id) {
                        $scope.entityId = id;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/equipo/edit.html',
                            controller: 'EquipoEditController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (data) {
                            requestService.send('api/v1/equipo/edit', 'put', { data: data })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                            if (data.data.errors) {
                                                data.data.errors.forEach(function(error){
                                                    notificacion('error', error.message);
                                                });
                                            }
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalFotos = function (id) {
                        $scope.idEquipo = id;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/equipo/fotos.html',
                            controller: 'EquipoFotosController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (equipo) {
                            loadingService.on();
                            requestService.send('api/v1/equipo/fotos', 'post', { data: equipo })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                            if (data.data.errors) {
                                                data.data.errors.forEach(function(error){
                                                    notificacion('error', error.message);
                                                });
                                            }
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };

                    requestService.send('api/v1/equipo')
                        .then(
                            function (data) {
                                $scope.entities = data.data.equipos;
                                $scope.admin = data.data.admin;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                    //$location.path('/login')
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                    //$location.path('/login')
                                }
                                loadingService.off();
                            }
                        );
                }
        )
        .controller("EquipoEditController",
            function (loadingService, requestService, $scope, configService) {
                loadingService.on();
                $scope.guardar = function () {
                    $uibModalInstance.close($scope.entity);
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                configService.checkLogin();
                var id = $scope.entityId;
                requestService.send('api/v1/equipo/' + id)
                    .then(
                            function (data) {
                                $scope.entity = data.data;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                }
                                loadingService.off();
                            }
                    );
            }
        )
        .controller("EquipoFotosController", function(loadingService, requestService, $scope){
            var images = new Array();
            
            $scope.cerrar = function () {
                $uibModalInstance.close({equipo: $scope.idEquipo, fotos: images});
            };
            $scope.imagenes = function (img) {
                images.push(img);
            };
        })
        .controller("MisEquiposConfirmarController",
            function (loadingService, requestService, $scope, $location, $routeParams, configService) {
                loadingService.on();
                configService.checkLogin();
                var id = $scope.equipoId;
                var movimientoId = $scope.movimientoId
                $scope.url_imgs = URL_API;
                requestService.send('api/v1/equipo/mis/equipos/confirmar/' + id)
                    .then(
                        function (data) {
                            $scope.entity = data.data;
                            loadingService.off();
                        },
                        function (data) {

                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                    );
                requestService.send('api/v1/equipo/mis/equipos/datos_envio/' + movimientoId)
                    .then(
                        function (data) {
                            $scope.envio = data.data;
                            loadingService.off();
                        },
                        function (data) {

                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                    );
                requestService.send('api/v1/equipo/mis/equipos/estado/movimiento')
                    .then(
                        function (data) {
                            $scope.estadoMovimiento = data.data;
                            $scope.estado = $scope.estadoMovimiento[0];
                            loadingService.off();
                        },
                        function (data) {

                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                    );
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.confirmar = function () {
                    $uibModalInstance.close({ estado: $scope.estado, comentario: $scope.comentario });
                };
            }
        )
        .controller("MisEquiposEnviarController",
            function (loadingService, requestService, $scope, $location, configService, storageService, badgeEquiposService) {
                configService.checkLogin();
                loadingService.on();
                $scope.equipos = storageService.get('equiposSeleccionados'); 
                requestService.send('api/v1/usuario')
                        .then(
                                function (data) {
                                    $scope.funcionarios = data.data.filter( function(funcionario) {
                                        return funcionario.excluir
                                    } );
                                    $scope.destinatario = data.data[0];
                                    loadingService.off();
                                },
                                function (data) {

                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
                $scope.quitar = function (id) {
                    var equiposActualizados = new Array();
                    var equiposSeleccionados = storageService.get('equiposSeleccionados'); 
                    equiposSeleccionados.forEach(function(item, index) {
                        if (item.id !== id) {
                            equiposActualizados.push(item);
                        }
                    });
                    storageService.set('equiposSeleccionados', equiposActualizados);
                    badgeEquiposService.setPendientes(equiposActualizados.length);
                    if (equiposActualizados.length === 0) {
                        $location.path('/misequipos');
                    }
                    $scope.equipos = equiposActualizados;
                };
                $scope.enviar = function() {
                    var data = {
                        destinatarioId: $scope.destinatario.id,
                        empresaTransporte: $scope.empresaTransporte,
                        guia: $scope.guia,
                        comentario: $scope.comentario,
                        equipos: $scope.equipos 
                    };
                    requestService.send('api/v1/equipo/mis/equipos/enviar', 'post', { data: data })
                        .then(
                            function (data) {
                                var id = data.data.movimientoId;
                                notificacion('success', data.data.message);
                                loadingService.off();
                                sessionStorage.removeItem('equiposSeleccionados');
                                badgeEquiposService.setPendientes(0);
                                
                                requestService.send('api/v1/mail/notificacion/' + id)
                                    .then(
                                        function (data) {
                                            $location.path('/misequipos/resumen/' + id);  
                                            loadingService.off();
                                        },
                                        function (data) {
                                            $location.path('/misequipos/resumen/' + id);
                                            if (data.data && data.data.message) {
                                                notificacion('error', data.data.message);
                                                //$location.path('/login')
                                            } else {
                                                notificacion('error', 'A connection error was detected.');
                                                //$location.path('/login')
                                            }
                                            loadingService.off();
                                        }
                                    );
                            },
                            function (data) {
                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                }
                                loadingService.off();
                            }
                        );
                };
                /*                
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                $scope.confirmar = function () {
                    $uibModalInstance.close({ estado: $scope.estado, comentario: $scope.comentario });
                };*/
            }
        )
        .controller("MisEquiposController",
            function (badgeMailService, badgeEquiposService, loadingService, requestService, $scope, $location, $routeParams, configService, $uibModal, storageService) {
                loadingService.on();
                configService.checkLogin();
                requestService.send('api/v1/tecnica')
                        .then(
                                function (data) {
                                    $scope.tecnicas = data.data;  
                                    loadingService.off();
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
                requestService.send('api/v1/equipo/mis/equipos/0')
                        .then(
                                function (data) {
                                    $scope.equiposXconfirmar = data.data;  
                                    loadingService.off();
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
                requestService.send('api/v1/equipo/mis/equipos/1')
                        .then(
                                function (data) {
                                    $scope.equiposAcargo = data.data;  
                                    // TODO: equipos seleccionados para adicionar al envio
                                    $scope.equiposSeleccionados = storageService.get('equiposSeleccionados'); 
                                    loadingService.off();
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
                $scope.openModalConfirmar = function (id, movimientoId) {
                        $scope.equipoId = id;
                        $scope.movimientoId = movimientoId;
                        
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/misequipos/confirmar.html',
                            controller: 'MisEquiposConfirmarController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope,
                            size: 'lg'
                        });
                        $uibModalInstance.result.then(function (confirmacion) {
                            loadingService.on();
                            requestService.send('api/v1/equipo/mis/equipos/confirmar', 'post', { data: {equipoId: id, movimientoId: movimientoId, confirmacion: confirmacion } })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        if ( data.data.estadoEnvio === 5 || data.data.estadoEnvio ===7 ) {
                                            // enviar email de confirmación con alertta.
                                            var id = data.data.movimientoId;
                                            requestService.send('api/v1/mail/alerta/' + id)
                                                .then();
                                        }
                                        loadingService.off();                                        
                                        requestService.send('api/v1/equipo/mis/equipos/0')
                                                .then(
                                                        function (data) {
                                                            $scope.equiposXconfirmar = data.data;                                    
                                                            loadingService.off();
                                                        },
                                                        function (data) {
                                                            if (data.data && data.data.message) {
                                                                notificacion('error', data.data.message);
                                                                //$location.path('/login')
                                                            } else {
                                                                notificacion('error', 'A connection error was detected.');
                                                                //$location.path('/login')
                                                            }
                                                            loadingService.off();
                                                        }
                                                );
                                        requestService.send('api/v1/equipo/mis/equipos/1')
                                                .then(
                                                        function (data) {
                                                            $scope.equiposAcargo = data.data;
                                                            loadingService.off();
                                                        },
                                                        function (data) {
                                                            if (data.data && data.data.message) {
                                                                notificacion('error', data.data.message);
                                                                //$location.path('/login')
                                                            } else {
                                                                notificacion('error', 'A connection error was detected.');
                                                                //$location.path('/login')
                                                            }
                                                            loadingService.off();
                                                        }
                                                ); 
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                $scope.test = function() {
                    requestService.send('api/v1/equipo/mis/equipos/trazabilidad/1')
                        .then(
                                function (data) {
                                    //$scope.equiposAcargo = data.data;
                                    loadingService.off();
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        ); 
                };
                $scope.openModalDetalle = function (id) {
                    $scope.equipoId = id;
                    $uibModalInstance = $uibModal.open({
                        templateUrl: 'partials/equipo/detail.html',
                        controller: 'EquipoDetailController',
                        keyboard: false,
                        backdrop: true, //'static'
                        scope: $scope
                    });
                    $uibModalInstance.result.then(function (equipo) {
                        loadingService.on();
                        
                        }, function () {}
                    );
                };
                $scope.openModalAdicionar = function() {
                    storageService.set('equiposSeleccionados', $scope.equiposSeleccionados);
                    badgeEquiposService.setPendientes(storageService.len('equiposSeleccionados'));
                    notificacion('success', 'Hay pendientes <strong>' 
                            + storageService.len('equiposSeleccionados') + ' equipo(s)</strong> por despachar');
                };
            }
        )
        .controller("MisEquiposResumenController",
            function (loadingService, requestService, $scope, $routeParams, $location, configService, storageService, $window) {
                configService.checkLogin();
                var id = $routeParams.id;
                loadingService.on();
                $scope.equipos = storageService.get('equiposSeleccionados'); 
                requestService.send('api/v1/equipo/mis/equipos/resumen/' + id)
                    .then(
                        function (data) {
                            $scope.envio = data.data;
                            loadingService.off();
                        },
                        function (data) {

                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                    );
                $scope.continuar = function () {
                    $location.path('/remisiones')
                };
                $scope.imprimir = function (id) {
                    $window.open(URL_API + '/pdfs/remision/' + id + '/token');
                };
            }
        )
        .controller("RemisionesController",
            function (loadingService, requestService, $scope, $routeParams, $location, configService, storageService, badgeEquiposService, $uibModal) {
                configService.checkLogin();
                loadingService.on();
                requestService.send('api/v1/remision/listado')
                    .then(
                        function (data) {
                            $scope.remisiones = data.data;
                            loadingService.off();
                        },
                        function (data) {

                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                    );
                $scope.openModalDetalle = function (id) {
                    $scope.movimientoId = id;
                    $uibModalInstance = $uibModal.open({
                        templateUrl: 'partials/remisiones/detalle.html',
                        controller: 'RemisionesDetalleController',
                        keyboard: false,
                        backdrop: true, //'static'
                        scope: $scope,
                        size: 'lg'
                    });
                    $uibModalInstance.result.then(function (equipo) {
                        loadingService.on();
                        
                        }, function () {}
                    );
                };
                
            }
        )
        .controller("RemisionesDetalleController",
            function (loadingService, requestService, $scope, $routeParams, $location, configService, storageService, badgeEquiposService, $uibModal) {
                configService.checkLogin();
                loadingService.on();
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                var id = $scope.movimientoId;
                requestService.send('api/v1/remision/detalle/' + id)
                    .then(
                        function (data) {
                            $scope.remision = data.data;
                            loadingService.off();
                        },
                        function (data) {

                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                                //$location.path('/login')
                            } else {
                                notificacion('error', 'A connection error was detected.');
                                //$location.path('/login')
                            }
                            loadingService.off();
                        }
                    );
            }
        )
        .controller("SedeNewController", function($scope, loadingService, requestService){
            loadingService.on();
            $scope.ok = function () {
                $uibModalInstance.close($scope.sede);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
            requestService.send('api/v1/usuario')
                .then(
                    function (data) {
                        $scope.funcionarios = data.data;
                        loadingService.off();
                    },
                    function (data) {
                        if (data.data && data.data.message) {
                            notificacion('error', data.data.message);
                            //$location.path('/login')
                        } else {
                            notificacion('error', 'A connection error was detected.');
                            //$location.path('/login')
                        }
                        loadingService.off();
                    }
                );
        })
        .controller("SedeController",
                function (loadingService, configService, requestService, $scope, $route, $uibModal) {
                    loadingService.on();
                    configService.checkLogin();
                    $scope.openModalBorrar = function (entityId) {
                        $scope.entityId = entityId;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/common/confirmacionDelete.html',
                            controller: 'ConfirmacionDeleteController',
                            size: 'sm',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function () {
                            loadingService.on();
                            requestService.send('api/v1/sede/delete/' + $scope.entityId, 'delete')
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalNew = function () {
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/sede/new.html',
                            controller: 'SedeNewController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (sede) {
                            loadingService.on();
                            requestService.send('api/v1/sede/new', 'post', { data: sede })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                            if (data.data.errors) {
                                                data.data.errors.forEach(function(error){
                                                    notificacion('error', error.message);
                                                });
                                            }
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalEdit = function (id) {
                        $scope.entityId = id;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/sede/edit.html',
                            controller: 'SedeEditController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (data) {
                                requestService.send('api/v1/sede/edit', 'put', { data: data })
                                    .then(
                                        function (data) {
                                            notificacion('success', data.data.message);
                                            loadingService.off();
                                            $route.reload();
                                        },
                                        function (data) {
                                            if (data.data && data.data.message) {
                                                notificacion('error', data.data.message);
                                                if (data.data.errors) {
                                                    data.data.errors.forEach(function(error){
                                                        notificacion('error', error.message);
                                                    });
                                                }
                                            } else {
                                                notificacion('error', 'A connection error was detected.');
                                            }
                                            loadingService.off();
                                        }
                                    );
                            }, function () {}
                        );
                    };
                    requestService.send('api/v1/sede')
                        .then(
                            function (data) {
                                $scope.entities = data.data;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                    //$location.path('/login')
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                    //$location.path('/login')
                                }
                                loadingService.off();
                            }
                        );
                }
        )
        .controller("SedeEditController",
            function (loadingService, requestService, $scope, configService) {
                loadingService.on();
                $scope.guardar = function () {
                    $uibModalInstance.close({
                        'almacenista': $scope.usuarioSede, 
                        'sede': $scope.entity});
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                configService.checkLogin();
                var id = $scope.entityId;
                requestService.send('api/v1/sede/' + id)
                    .then(
                        function (data) {
                            $scope.entity = data.data;
                            loadingService.off();
                            
                            var opcion = data.data.usuarioSede;
                            loadingService.on();
                            requestService.send('api/v1/usuario')
                                .then(
                                        function (data) {
                                            var opciones = new Array();
                                            data.data.forEach(function (element) {
                                                opciones.push({
                                                    id: element.id,
                                                    nombre: element.nombre
                                                });
                                            });
                                            var datos = {
                                                opciones: opciones,
                                                opcion: opcion
                                            };
                                            loadingService.off();
                                            $scope.usuarioSede = datos;
                                        },
                                        function (data) {
                                            if (data.data && data.data.message) {
                                                notificacion('error', data.data.message);
                                            } else {
                                                notificacion('error', 'A connection error was detected.');
                                            }
                                            loadingService.off();
                                        }
                                );
                        },
                        function (data) {
                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                            } else {
                                notificacion('error', 'A connection error was detected.');
                            }
                            loadingService.off();
                        }
                    );
            }
        )
        .controller("TecnicaNewController", function($scope, loadingService, requestService){
            $scope.ok = function () {
                $uibModalInstance.close($scope.sede);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .controller("TecnicaController",
                function (loadingService, configService, requestService, $scope, $route, $uibModal) {
                    loadingService.on();
                    configService.checkLogin();
                    $scope.openModalBorrar = function (entityId) {
                        $scope.entityId = entityId;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/common/confirmacionDelete.html',
                            controller: 'ConfirmacionDeleteController',
                            size: 'sm',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function () {
                            loadingService.on();
                            requestService.send('api/v1/tecnica/delete/' + $scope.entityId, 'delete')
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalNew = function () {
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/tecnica/new.html',
                            controller: 'TecnicaNewController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (sede) {
                            loadingService.on();
                            requestService.send('api/v1/tecnica/new', 'post', { data: sede })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                            if (data.data.errors) {
                                                data.data.errors.forEach(function(error){
                                                    notificacion('error', error.message);
                                                });
                                            }
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalEdit = function (id) {
                        $scope.entityId = id;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/tecnica/edit.html',
                            controller: 'TecnicaEditController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (data) {
                                requestService.send('api/v1/tecnica/edit', 'put', { data: data })
                                    .then(
                                        function (data) {
                                            notificacion('success', data.data.message);
                                            loadingService.off();
                                            $route.reload();
                                        },
                                        function (data) {
                                            if (data.data && data.data.message) {
                                                notificacion('error', data.data.message);
                                                if (data.data.errors) {
                                                    data.data.errors.forEach(function(error){
                                                        notificacion('error', error.message);
                                                    });
                                                }
                                            } else {
                                                notificacion('error', 'A connection error was detected.');
                                            }
                                            loadingService.off();
                                        }
                                    );
                            }, function () {}
                        );
                    };

                    requestService.send('api/v1/tecnica')
                        .then(
                            function (data) {
                                $scope.entities = data.data;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                    //$location.path('/login')
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                    //$location.path('/login')
                                }
                                loadingService.off();
                            }
                        );
                }
        )
        .controller("TecnicaEditController",
            function (loadingService, requestService, $scope, configService) {
                loadingService.on();
                $scope.guardar = function () {
                    $uibModalInstance.close($scope.entity);
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                configService.checkLogin();
                var id = $scope.entityId;
                requestService.send('api/v1/tecnica/' + id)
                    .then(
                        function (data) {
                            $scope.entity = data.data;
                            loadingService.off();
                        },
                        function (data) {
                            if (data.data && data.data.message) {
                                notificacion('error', data.data.message);
                            } else {
                                notificacion('error', 'A connection error was detected.');
                            }
                            loadingService.off();
                        }
                    );
            }
        )
        .controller("TecnicaEquiposController",
            function (loadingService, requestService, $scope, $location, $routeParams, configService, $uibModal) {
                loadingService.on();
                $scope.tecnica = {};
                configService.checkLogin();
                var idTecnica = $routeParams.id;
                $scope.atras = function() {
                    $location.path('/tecnica');
                };
                $scope.guardar = function() {
                    requestService.send('api/v1/tecnica/equipos/update', 'post', {
                        idTecnica: idTecnica,
                        equipos: $scope.tecnica.equipos
                    })
                        .then(
                                function (data) {
                                    notificacion('success', data.data.message);
                                    loadingService.off();
                                    $location.path('/tecnica');
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
                    
                };
                $scope.openModalDetalle = function (id) {
                    $scope.equipoId = id;
                    $uibModalInstance = $uibModal.open({
                        templateUrl: 'partials/equipo/detail.html',
                        controller: 'EquipoDetailController',
                        keyboard: false,
                        backdrop: true, //'static'
                        scope: $scope
                    });
//                    $uibModalInstance.result.then(function (equipo) {
//                        loadingService.on();
//                        requestService.send('api/v1/equipo/new', 'post', { data: equipo })
//                            .then(
//                                function (data) {
//                                    notificacion('success', data.data.message);
//                                    loadingService.off();
//                                    $route.reload();
//                                },
//                                function (data) {
//                                    if (data.data && data.data.message) {
//                                        notificacion('error', data.data.message);
//                                    } else {
//                                        notificacion('error', 'A connection error was detected.');
//                                    }
//                                    loadingService.off();
//                                }
//                            );
//                        }, function () {}
//                    );
                };
                requestService.send('api/v1/tecnica/' + idTecnica + '/equipos')
                        .then(
                                function (data) {
                                    $scope.equipos = data.data;
                                    var equipos = new Array();
                                    data.data.forEach(function(item, index){
                                        if (item.pertenece) {
                                            equipos.push(item);
                                        }
                                    });
                                    $scope.tecnica.equipos = equipos;
                                    loadingService.off();
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
                requestService.send('api/v1/tecnica/' + idTecnica )
                        .then(
                                function (data) {
                                    $scope.tecnica.nombre = data.data.nombre;                                    
                                    loadingService.off();
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                        //$location.path('/login')
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                        //$location.path('/login')
                                    }
                                    loadingService.off();
                                }
                        );
            }
        )
        .controller("UsuarioNewController", function(loadingService, requestService, $scope){
            loadingService.on();
            requestService.send('api/v1/usuario')
                .then(
                    function (data) {
                        $scope.funcionarios = data.data;
                        loadingService.off();
                    },
                    function (data) {

                        if (data.data && data.data.message) {
                            notificacion('error', data.data.message);
                            //$location.path('/login')
                        } else {
                            notificacion('error', 'A connection error was detected.');
                            //$location.path('/login')
                        }
                        loadingService.off();
                    }
                );
            requestService.send('api/v1/sede')
                .then(
                    function (data) {
                        $scope.sedes = data.data;
                        loadingService.off();
                    },
                    function (data) {
                        if (data.data && data.data.message) {
                            notificacion('error', data.data.message);
                            //$location.path('/login')
                        } else {
                            notificacion('error', 'A connection error was detected.');
                            //$location.path('/login')
                        }
                        loadingService.off();
                    }
                );
            $scope.ok = function () {
                $uibModalInstance.close($scope.usuario);
            };
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        })
        .controller("UsuarioController",
                function (loadingService, configService, requestService, $scope, $route, $uibModal) {
                    loadingService.on();
                    configService.checkLogin();
                    $scope.openModalBorrar = function (entityId) {
                        $scope.entityId = entityId;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/common/confirmacionDelete.html',
                            controller: 'ConfirmacionDeleteController',
                            size: 'sm',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function () {
                            loadingService.on();
                            requestService.send('api/v1/usuario/delete/' + $scope.entityId, 'delete')
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalNew = function () {
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/usuario/new.html',
                            controller: 'UsuarioNewController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (usuario) {
                            loadingService.on();
                            requestService.send('api/v1/usuario/new', 'post', { data: usuario })
                                .then(
                                    function (data) {
                                        notificacion('success', data.data.message);
                                        loadingService.off();
                                        $route.reload();
                                    },
                                    function (data) {
                                        if (data.data && data.data.message) {
                                            notificacion('error', data.data.message);
                                            if (data.data.errors) {
                                                data.data.errors.forEach(function(error){
                                                    notificacion('error', error.message);
                                                });
                                            }
                                        } else {
                                            notificacion('error', 'A connection error was detected.');
                                        }
                                        loadingService.off();
                                    }
                                );
                            }, function () {}
                        );
                    };
                    $scope.openModalEdit = function (id) {
                        $scope.entityId = id;
                        $uibModalInstance = $uibModal.open({
                            templateUrl: 'partials/usuario/edit.html',
                            controller: 'UsuarioEditController',
                            keyboard: false,
                            backdrop: true, //'static'
                            scope: $scope
                        });
                        $uibModalInstance.result.then(function (data) {
                                requestService.send('api/v1/usuario/edit', 'put', { data: data })
                                    .then(
                                        function (data) {
                                            notificacion('success', data.data.message);
                                            loadingService.off();
                                            $route.reload();
                                        },
                                        function (data) {
                                            if (data.data && data.data.message) {
                                                notificacion('error', data.data.message);
                                                if (data.data.errors) {
                                                    data.data.errors.forEach(function(error){
                                                        notificacion('error', error.message);
                                                    });
                                                }
                                            } else {
                                                notificacion('error', 'A connection error was detected.');
                                            }
                                            loadingService.off();
                                        }
                                    );
                            }, function () {}
                        );
                    };
                    $scope.reset = function (id) {
                        requestService.send('api/v1/usuario/password', 'put', {id: id, password: "123456", cambiarPass: true})
                            .then(
                                function (data) {
                                    notificacion('success', data.data.message);
                                },
                                function (data) {
                                    if (data.data && data.data.message) {
                                        notificacion('error', data.data.message);
                                    } else {
                                        notificacion('error', 'A connection error was detected.');
                                    }
                                }
                            );
                    };
                    
                    requestService.send('api/v1/usuario')
                        .then(
                            function (data) {
                                $scope.entities = data.data;
                                loadingService.off();
                            },
                            function (data) {

                                if (data.data && data.data.message) {
                                    notificacion('error', data.data.message);
                                    //$location.path('/login')
                                } else {
                                    notificacion('error', 'A connection error was detected.');
                                    //$location.path('/login')
                                }
                                loadingService.off();
                            }
                        );
                }
        )
        .controller("UsuarioEditController",
            function (loadingService, requestService, $scope, configService, $q) {
                
                loadingService.on();
                $scope.guardar = function () {
                    $uibModalInstance.close({
                        'sede': $scope.sede, 
                        'jefe': $scope.jefe, 
                        'funcionario': $scope.entity});
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
                configService.checkLogin();
                var id = $scope.entityId;
                $q.all([
                    requestService.send('api/v1/usuario/' + id) ,
                    requestService.send('api/v1/sede'),
                    requestService.send('api/v1/usuario')
                ]).then(function (res) {
                    $scope.entity = res[0].data;
                    var sede = res[0].data.sede;
                    var jefe = res[0].data.jefe;
                    var opciones = new Array();
                    res[1].data.forEach(function (element) {
                        opciones.push({
                            id: element.id,
                            nombre: element.nombre
                        });
                    });
                    var datos = {
                        opciones: opciones,
                        opcion: sede
                    };
                    $scope.sede = datos;
                    
                    opciones = new Array();
                    res[2].data.forEach(function (element) {
                        opciones.push({
                            id: element.id,
                            nombre: element.nombre
                        });
                    });
                    var datos = {
                        opciones: opciones,
                        opcion: jefe
                    };
                    $scope.jefe = datos;
                    loadingService.off();
                });
            }
        )
;
